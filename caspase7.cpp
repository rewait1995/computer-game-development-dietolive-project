#include "caspase7.h"

Caspase7::Caspase7() : Entity()
{
    spriteData.width = caspase7NS::WIDTH;           // size of Ship1
    spriteData.height = caspase7NS::HEIGHT;
    spriteData.x = caspase7NS::X;                   // location on screen
    spriteData.y = caspase7NS::Y;
    spriteData.rect.bottom = caspase7NS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = caspase7NS::WIDTH;
    velocity.x = 1;                             // velocity X
    velocity.y = 1;                             // velocity Y
    radius = caspase7NS::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Caspase7::update(float frameTime)
{
    Entity::update(frameTime);

	if(getX() >= (GAME_WIDTH)-(getWidth()*getScale()))
	{
		setX((GAME_WIDTH)-(getWidth()*getScale()));
	}
	if(getY() >= GAME_HEIGHT)
	{
		setY((GAME_HEIGHT)-(getHeight()*getScale()));
	}
	setX(getX() + velocity.x*frameTime);
	setY(getY() + velocity.y*frameTime);
    // Bounce off walls
    // if hit right screen edge
    if (spriteData.x > GAME_WIDTH-caspase7NS::WIDTH*getScale())
    {
        // position at right screen edge
        spriteData.x = GAME_WIDTH-caspase7NS::WIDTH*getScale();
        velocity.x = -velocity.x;               // reverse X direction
    } 
    else if (spriteData.x < 0)                  // else if hit left screen edge
    {
        spriteData.x = 0;                       // position at left screen edge
        velocity.x = -velocity.x;               // reverse X direction
    }
    // if hit bottom screen edge
    if (spriteData.y > GAME_HEIGHT-caspase7NS::HEIGHT*getScale())
    {
        // position at bottom screen edge
        spriteData.y = GAME_HEIGHT-caspase7NS::HEIGHT*getScale();
        velocity.y = -velocity.y;               // reverse Y direction
    }
    else if (spriteData.y < 0)                  // else if hit top screen edge
    {
        spriteData.y = 0;                       // position at top screen edge
        velocity.y = -velocity.y;               // reverse Y direction
    }
}