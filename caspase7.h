/*Author: Michelle Chu, Rachel Waitverlech
  Year: 2016
  Caspase class representing both activated and unactivated executioner*/
#ifndef _CASPASE7_H
#define _CASPASE7_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace caspase7NS
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 128;                  // image height
    const int X = GAME_WIDTH;   // location on screen
    const int Y = GAME_HEIGHT;
    const float SPEED = 100;                // 100 pixels per second
    const float MASS = 300.0f;              // mass
    const int   TEXTURE_COLS = 1;           // texture has 2 columns
}

// inherits from Entity class
class Caspase7 : public Entity
{
public:
    // constructor
    Caspase7();

    // inherited member functions
    void update(float frameTime);
};

#endif