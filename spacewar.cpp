//Heavily modified by Michelle Chu Rachel Waitverlech from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly
// Draw animated apoptosome
// This class is the core of the game

#include "spaceWar.h"

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{
	//initiating array of unactivated caspase pointers
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		caspase_unactivated[i] = new Caspase7;
	}
	//initiating array of activated caspase pointers
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		caspase_activated[i] = new Caspase7;
	}

	textPtr	= new TextDX();
	//timer parameters
	seconds = 0;
	minutes = 2;
	//win condition
	allCaspasesActivated = false;
	endWin = false;
	endLose = false;
	lastIntroFrame = false;
	bool playWin = true;
	bool playLose = true;
	bool playBackground = true;
	frameCounter = 0;
}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
	//deleting pointers
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		delete caspase_unactivated[i];
		delete caspase_activated[i];
	}
    releaseAll();           // call onLostDevice() for every graphics item

	SAFE_DELETE(textPtr);
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError

	// initialize DirectX fonts
    // 15 pixel high Arial
    if(textPtr->initialize(graphics, 100, true, false, "Arial") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

    // cell interior texture
    if (!cellInteriorTexture.initialize(graphics,CELL_INSIDE_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula texture"));
    // apoptosome texture
    if (!apoptosomeTexture.initialize(graphics,APOPTOSOME_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing apoptosome texture"));
	// partying cells texture
	if (!partyTexture.initialize(graphics,PARTYING_CELLS))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing partying texture"));
	// poof texture
    if (!poofTexture.initialize(graphics,POOF_CANCER_CELL))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing poof texture"));
	// intro screen texture
	if (!introTexture.initialize(graphics,INTRO_SCREEN))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing intro screen texture"));
	// lose screen texture
	if (!loseScreenTexture.initialize(graphics,LOSE_SCREEN))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing apoptosome texture"));
	// instruction texture
	if (!instructionScreenTexture.initialize(graphics,INSTRUCTION_SCREEN))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing apoptosome texture"));

    // cell interior image
    if (!cellInterior.initialize(graphics,0,0,0,&cellInteriorTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing cell background"));
    // apoptosome
    if (!apoptosome.initialize(this, apoptosomeNS::WIDTH, apoptosomeNS::HEIGHT, apoptosomeNS::TEXTURE_COLS, &apoptosomeTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing apoptosome"));
	//poof image
    if (!poofCancer.initialize(graphics,0,0,0,&poofTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing poof"));
	//partying image
    if (!partyCells.initialize(graphics,0,0,0,&partyTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing partying image"));
	//intro image
    if (!introScreen.initialize(graphics,0,0,0,&introTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing intro screen"));
	//lose screen image
    if (!loseScreen.initialize(graphics,0,0,0,&loseScreenTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing lose screen"));
	//instruction image
	if (!instructionScreen.initialize(graphics,0,0,0,&instructionScreenTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing lose screen"));

	// caspase-unactivated texture
	if (!caspaseUnActTexture.initialize(graphics,CASPASE_UNACT_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing unactivated caspase texture"));

	//caspase-activated texture
	if(!caspaseActTexture.initialize(graphics, CASPASE_ACT_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing activated caspase texture"));

	//initializing unactivated caspase textures per array element
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		if(!caspase_unactivated[i]->initialize(this, caspase7NS::WIDTH, caspase7NS::HEIGHT, caspase7NS::TEXTURE_COLS, &caspaseUnActTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing caspase texture"));
		caspase_unactivated[i]->setX(GAME_WIDTH/(rand()%10+1));
		caspase_unactivated[i]->setY(GAME_HEIGHT/(rand()%10+1));
		caspase_unactivated[i]->setVisible(true);
		caspase_unactivated[i]->setVelocity(VECTOR2((rand()%5+1)*caspase7NS::SPEED, (rand()%5+1)*-caspase7NS::SPEED));
	}
	
	//initializing activated caspase textures per array element
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		if(!caspase_activated[i]->initialize(this, caspase7NS::WIDTH, caspase7NS::HEIGHT, caspase7NS::TEXTURE_COLS, &caspaseActTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing caspase texture"));
		caspase_activated[i]->setX(GAME_WIDTH/(rand()%10+1));
		caspase_activated[i]->setY(GAME_HEIGHT/(rand()%10+1));
		caspase_activated[i]->setVisible(false);
		caspase_activated[i]->setVelocity(VECTOR2((rand()%5+1)*caspase7NS::SPEED, (rand()%5+1)*-caspase7NS::SPEED));
	}

	//setting initial position of apoptosome
	apoptosome.setX(GAME_WIDTH/2 - apoptosomeNS::WIDTH);
	apoptosome.setY(GAME_HEIGHT/2 - apoptosomeNS::HEIGHT);

	//blebbing texture intialization
    if (!cancerTexture.initialize(graphics,CANCER_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing jpo texture"));

	//blebbing image
    if (!cancer.initialize(graphics,CANCER_WIDTH, CANCER_HEIGHT, CANCER_COLS, &cancerTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing jpo"));

    cancer.setX(800);                    // start above and left of dying cancer cell
    cancer.setY(30);
    cancer.setFrames(CANCER_START, CANCER_END);   // animation frames
    cancer.setCurrentFrame(CANCER_START);     // starting frame
    cancer.setFrameDelay(CANCER_ANIMATION_DELAY);



    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{
	if(input->isKeyDown(VK_ESCAPE))
	{
		PostQuitMessage(0);
	}

	// all caspases are initially unactivated
	bool allVisibleCheck = true;

	seconds -= frameTime;

	if(seconds <= 0)
	{
		minutes--;
		seconds = 60;
	}
	
	//update stuff
	 apoptosome.update(frameTime);
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		caspase_unactivated[i]->update(frameTime);
		caspase_activated[i]->update(frameTime);

		if(!(caspase_activated[i]->getVisible()))
		{
			allVisibleCheck = false;
		}
	}

	//If unactivated caspase is within cleavage radius and space is pressed, becomes activated
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		if((caspase_unactivated[i]->getX()+caspase_unactivated[i]->getRadius()) > (apoptosome.getX()+apoptosome.getRadius()-CLEAVAGE_SPECIFICITY)
			&&(caspase_unactivated[i]->getX()+caspase_unactivated[i]->getRadius()) < (apoptosome.getX()+apoptosome.getRadius()+CLEAVAGE_SPECIFICITY) 
			&&(caspase_unactivated[i]->getY()+caspase_unactivated[i]->getRadius()) > (apoptosome.getY()+apoptosome.getRadius()-CLEAVAGE_SPECIFICITY)
			&&(caspase_unactivated[i]->getY()+caspase_unactivated[i]->getRadius()) < (apoptosome.getY()+apoptosome.getRadius()+CLEAVAGE_SPECIFICITY)
			&&(input->wasKeyPressed(VK_SPACE))&&(caspase_unactivated[i]->getVisible() == true))
		{
			caspase_activated[i]->setX(caspase_unactivated[i]->getX());
			caspase_activated[i]->setY(caspase_activated[i]->getY());
			caspase_unactivated[i]->setVisible(false);
			caspase_activated[i]->setVisible(true);
		}
	}

	//If unactivated caspase is within cleavage radius and space is pressed, becomes activated
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		if((caspase_activated[i]->getX()+caspase_activated[i]->getRadius()) > (apoptosome.getX()+apoptosome.getRadius()-CLEAVAGE_SPECIFICITY)
			&&(caspase_activated[i]->getX()+caspase_activated[i]->getRadius()) < (apoptosome.getX()+apoptosome.getRadius()+CLEAVAGE_SPECIFICITY) 
			&&(caspase_activated[i]->getY()+caspase_activated[i]->getRadius()) > (apoptosome.getY()+apoptosome.getRadius() - CLEAVAGE_SPECIFICITY)
			&&(caspase_activated[i]->getY()+caspase_activated[i]->getRadius()) < (apoptosome.getY()+apoptosome.getRadius()+CLEAVAGE_SPECIFICITY)
			&& (input->wasKeyPressed(VK_SPACE))&&(caspase_activated[i]->getVisible() == true))
		{
			caspase_unactivated[i]->setX(caspase_activated[i]->getX());
			caspase_unactivated[i]->setY(caspase_activated[i]->getY());
			caspase_activated[i]->setVisible(false);
			caspase_unactivated[i]->setVisible(true);
		}
	}
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacewar::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{
	VECTOR2 collisionVector;
    // if collision between two caspases
	for(int i = 0; i < NUMBER_CASPASE; i++)
	{
		for(int j = 0; j < NUMBER_CASPASE; j++)
		{
			if(caspase_unactivated[i]->collidesWith(*caspase_unactivated[j], collisionVector))
			{
				// caspases bounce off each other
				caspase_unactivated[i]->bounce(collisionVector, *caspase_unactivated[j]);
				caspase_unactivated[j]->bounce(collisionVector*-1, *caspase_unactivated[j]);
			}
		}
	}
	for(int i = 0; i<NUMBER_CASPASE; i++)
	{
		for(int j = 0; j < NUMBER_CASPASE; j++)
		{
			if(caspase_activated[i]->collidesWith(*caspase_activated[j], collisionVector))
			{
				// caspases bounce off each other
				caspase_activated[i]->bounce(collisionVector, *caspase_activated[j]);
				caspase_activated[j]->bounce(collisionVector*-1, *caspase_activated[j]);
			}
		}
	}
}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
    graphics->spriteBegin();                // begin drawing sprites

	if(!lastIntroFrame)
	{
		frameCounter++;
		if(frameCounter == 5)
		{
			audio->playCue(ROARING);
		}
		
		displayIntroScreen();
	}
	else
	{
		if((frameCounter == 5) && (!endLose) && (!endWin))
		{
			audio->playCue(INSTRUCTION);
			frameCounter = 0;
		}

		if((((int)minutes == 0) && ((int)seconds == 0)))
		{
			endLose = true;
		}

		if(endLose)
		{
			loseScreen.draw();
			//start lose sound in first frame
			if(playLose)
			{
				audio->stopCue(BACKGROUND);
				audio->playCue(LOSE);
				playLose = false;
			}

			if(input->isKeyDown(VK_ESCAPE))
			{
				PostQuitMessage(0);
			}
		}

		allCaspasesActivated = true;

		for(int i = 0; i < NUMBER_CASPASE; i++)
		{
			if(!(caspase_activated[i]->getVisible()))
			{
				allCaspasesActivated = false;
			}
		}

		if(allCaspasesActivated)
		{
			endWin = true;
			displayWinScreen();
		}

		if (!endLose && !allCaspasesActivated && !endWin)
		{
			cellInterior.draw();   // add the cell interior background to the scene

			for(int i = 0; i < NUMBER_CASPASE; i++)
			{
				if(caspase_unactivated[i]->getVisible() == true)
					caspase_unactivated[i]->draw();
				if(caspase_activated[i]->getVisible() == true)
					caspase_activated[i]->draw();
			}

			apoptosome.draw();                            // add the apoptosome to the scene

			if((input->wasKeyPressed(VK_SPACE)))
			{
				audio->playCue(APOP_CUT);
			}

			textPtr->setFontColor(graphicsNS::WHITE);
			if(seconds < 10)
			{
				textPtr->print(std::to_string(minutes) + ":0" + std::to_string((int)seconds),1100,30);
			}
			else
			{
				textPtr->print(std::to_string(minutes) + ":" + std::to_string((int)seconds),1100,30);
			}
		}
	}

    graphics->spriteEnd();                  // end drawing sprites
}

void Spacewar::displayWinScreen()
{
	frameCounter++;

	if (frameCounter <= 700)
	{
		poofCancer.draw();
		cancer.draw();
		cancer.setFrames(CANCER_START, CANCER_END);
		cancer.update(frameTime);
	}
	else
	{
		partyCells.draw();
		//start win sound in first frame
		if(playWin)
		{
			audio->stopCue(BACKGROUND);
			audio->playCue(WIN);
			playWin = false;
		}
	}

	if(input->isKeyDown(VK_ESCAPE))
	{
		PostQuitMessage(0);
	}
}

void Spacewar::displayIntroScreen()
{
	if (frameCounter <= 800)
	{
		introScreen.draw();
	}
	else
	{
		instructionScreen.draw();
		if(playBackground)
		{
			audio->playCue(BACKGROUND);
			playBackground = false;
		}
		if(input->wasKeyPressed(VK_RETURN))
		{
			lastIntroFrame = true;
		}
	}

	if(lastIntroFrame)
	{
		frameCounter = 5;
	}

}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
    apoptosomeTexture.onLostDevice();
    cellInteriorTexture.onLostDevice();
	caspaseActTexture.onLostDevice();
	caspaseUnActTexture.onLostDevice();
	poofTexture.onLostDevice();
	cancerTexture.onLostDevice();
	partyTexture.onLostDevice();
	introTexture.onLostDevice();
	loseScreenTexture.onLostDevice();

	textPtr->onLostDevice();

    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
    cellInteriorTexture.onResetDevice();
    apoptosomeTexture.onResetDevice();
	caspaseActTexture.onResetDevice();
	caspaseUnActTexture.onResetDevice();
	poofTexture.onResetDevice();
	cancerTexture.onResetDevice();
	partyTexture.onResetDevice();
	introTexture.onResetDevice();
	loseScreenTexture.onResetDevice();

	textPtr->onResetDevice();

    Game::resetAll();
    return;
}
