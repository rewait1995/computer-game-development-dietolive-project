// Heavily modified from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly

#ifndef _CONSTANTS_H            // Prevent multiple definitions if this 
#define _CONSTANTS_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }
// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr)    { if(ptr) { ptr->onLostDevice(); } }
// Safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr)   { if(ptr) { ptr->onResetDevice(); } }
#define TRANSCOLOR  SETCOLOR_ARGB(0,255,0,255)  // transparent color (magenta)

//-----------------------------------------------
//                  Constants
//-----------------------------------------------
// graphic images
const char CELL_INSIDE_IMAGE[] = "pictures\\Background.jpg";  // photo source NASA/courtesy of nasaimages.org 
const char APOPTOSOME_IMAGE[]  = "pictures\\apoptosome256_bright.png";  // spaceship
const char CASPASE_UNACT_IMAGE[] = "pictures\\caspase7_unact128.png"; // picture of unactivated executioner caspase
const char CASPASE_ACT_IMAGE[] = "pictures\\caspase7_act128.png";
const char POOF_CANCER_CELL[] = "pictures\\poof cancer cell.png";
const char INTRO_SCREEN[] = "pictures\\intro.png";
const char LOSE_SCREEN[] = "pictures\\lose spreading.png";
const char PARTYING_CELLS[] = "pictures\\party.png";
const char INSTRUCTION_SCREEN[] = "pictures\\Intro_screen.jpg";

// window
const char CLASS_NAME[] = "Die to Live";
const char GAME_TITLE[] = "Die to Live";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  1300;               // width of game in pixels
const UINT GAME_HEIGHT = 700;               // height of game in pixels


// graphic images
const char CANCER_IMAGE[]   = "pictures\\cancer.png"; //ship.png";   // spaceship
const float CANCER_ANIMATION_DELAY = 0.4f;    // time between frames of ship animation

//CANCER Cel
const int  CANCER_COLS = 4;
const int  CANCER_WIDTH = 256;
const int  CANCER_HEIGHT = 256;
//CANCER actions
const int CANCER_START = 0;			//1st row
const int CANCER_END = 8;

const float CANCER_SPEED = 10.0f;

// game
const double PI = 3.14159265;
const float FRAME_RATE = 200.0f;                // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations
const float GRAVITY = 6.67428e-11f;             // gravitational constant
const float CLEAVAGE_SPECIFICITY = 80;
const int NUMBER_CASPASE = 12;

// audio files required by audio.cpp
// WAVE_BANK must be location of .xwb file.
const char WAVE_BANK[]  = "audio\\Win\\Wave Bank.xwb";
// SOUND_BANK must be location of .xsb file.
const char SOUND_BANK[] = "audio\\Win\\Sound Bank.xsb";

// audio cues
//must be the  same as in XACT
const char BACKGROUND[] = "Apop_game_background_loud";
const char APOP_CUT[] = "apopCut_soft";
const char INSTRUCTION[] = "Stauff_activate";
const char ROARING[] = "DrB_angryCancerCell";
const char WIN[] = "cheer";
const char LOSE[] = "Sad Trombone";

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;       // escape key
const UCHAR ALT_KEY      = VK_MENU;         // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;       // Enter key

#endif
