// Heavily modified by Michelle Chu Rachel Rachel Waitverlech from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly
//apoptosome class 

#include "apoptosome.h"

//=============================================================================
// default constructor
//=============================================================================
Apoptosome::Apoptosome() : Entity()
{
    spriteData.width = apoptosomeNS::WIDTH;           // size of Ship1
    spriteData.height = apoptosomeNS::HEIGHT;
    spriteData.x = apoptosomeNS::X;                   // location on screen
    spriteData.y = apoptosomeNS::Y;
    spriteData.rect.bottom = apoptosomeNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = apoptosomeNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    radius = apoptosomeNS::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Apoptosome::update(float frameTime)
{
    Entity::update(frameTime);
    spriteData.angle += frameTime * apoptosomeNS::ROTATION_RATE;  // rotate the ship

	int directionX = 0;
	int directionY = 0;

	if((input->isKeyDown(VK_RIGHT)))
		directionX++;
	if((input->isKeyDown(VK_LEFT)))
		directionX--;
	if((input->isKeyDown(VK_UP)))
		directionY--;
	if((input->isKeyDown(VK_DOWN)))
		directionY++;

	setX(getX() + (apoptosomeNS::SPEED*directionX*frameTime));
	setY(getY() + (apoptosomeNS::SPEED*directionY*frameTime));
	if(getX() >= (GAME_WIDTH)-(getWidth()*getScale()))
	{
		setX((GAME_WIDTH)-(getWidth()*getScale()));
	}
	if(getY() >= GAME_HEIGHT)
	{
		setY((GAME_HEIGHT)-(getHeight()*getScale()));
	}
    // Bounce off walls
    // if hit right screen edge
    if (spriteData.x > GAME_WIDTH-apoptosomeNS::WIDTH*getScale())
    {
        // position at right screen edge
        spriteData.x = GAME_WIDTH-apoptosomeNS::WIDTH*getScale();
        velocity.x = -velocity.x;               // reverse X direction
    } 
    else if (spriteData.x < 0)                  // else if hit left screen edge
    {
        spriteData.x = 0;                       // position at left screen edge
        velocity.x = -velocity.x;               // reverse X direction
    }
    // if hit bottom screen edge
    if (spriteData.y > GAME_HEIGHT-apoptosomeNS::HEIGHT*getScale())
    {
        // position at bottom screen edge
        spriteData.y = GAME_HEIGHT-apoptosomeNS::HEIGHT*getScale();
        velocity.y = -velocity.y;               // reverse Y direction
    }
    else if (spriteData.y < 0)                  // else if hit top screen edge
    {
        spriteData.y = 0;                       // position at top screen edge
        velocity.y = -velocity.y;               // reverse Y direction
    }
}
