// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 spacewar.h v1.0

#ifndef _SPACEWAR_H             // Prevent multiple definitions if this 
#define _SPACEWAR_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "planet.h"
#include "apoptosome.h"
#include "caspase7.h"
#include "textDX.h"

//=============================================================================
// This class is the core of the game
//=============================================================================
class Spacewar : public Game
{
private:
    // game items
    TextureManager cellInteriorTexture;   // nebula texture
    TextureManager apoptosomeTexture;     // ship texture
    Apoptosome    apoptosome;           // spaceship
    Planet  planet;         // the planet
    Image   cellInterior;         // backdrop image
	TextureManager caspaseUnActTexture;   // caspase-activated texture
	Caspase7* caspase_unactivated[NUMBER_CASPASE]; // array of pointers each to an unactivated caspase
	TextureManager caspaseActTexture;
	Caspase7* caspase_activated[NUMBER_CASPASE];

	TextureManager poofTexture;
	Image poofCancer;
	TextureManager partyTexture;
	Image partyCells;
	TextureManager introTexture;
	Image introScreen;
	TextureManager loseScreenTexture;
	Image loseScreen;
	TextureManager instructionScreenTexture;
	Image instructionScreen;

	TextureManager cancerTexture;     // ship texture
    Image  cancer;                   // ship image
	enum LastDirection {left, right} lastDirection;

	
	TextDX  *textPtr;
	int minutes;
	float seconds;
	bool allCaspasesActivated;
	bool endWin;
	bool endLose;
	int frameCounter;
	bool lastIntroFrame;
	bool playWin;
	bool playLose;
	bool playBackground;

	//float fracSecondTime;
	//int		timeCounter;
	void displayWinScreen();
	void displayIntroScreen();

public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

#endif
